INTRODUCTION
------------

This module provides restriction type plugin for Date Restrictions module.
'Relate date: Weekdays' restriction type allows you to restrict date by
relative date for business day. When limit date by relative date, This excludes
weekend days(ex. Sunday and Saturday) of restricted duration and append a count of
weekend days to duraton.

REQUIREMENTS
------------

This module requires the following modules:

* Date Restrictions (https://drupal.org/project/date_restrictions)
* Weekdays (https://drupal.org/project/weekdays)

INSTALLATION
------------

* Install as usual, see https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

* You likely want to disable Toolbar module, since its output clashes with
  Administration menu.

CONFIGURATION
-------------

 * Create or edit a date field.
 * Configure under "More settings and values" > "Restrictions".
 * Expand minimum or maximum date and choose restriction type to 'Relative date: Weekdays'.
